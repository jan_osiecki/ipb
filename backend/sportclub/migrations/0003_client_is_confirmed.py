# Generated by Django 3.0.6 on 2020-05-29 16:51

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('sportclub', '0002_auto_20200524_1518'),
    ]

    operations = [
        migrations.AddField(
            model_name='client',
            name='is_confirmed',
            field=models.BooleanField(default=True),
        ),
    ]
