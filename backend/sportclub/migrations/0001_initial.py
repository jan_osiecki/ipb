# Generated by Django 3.0.6 on 2020-05-21 07:44

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Client',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.TextField()),
                ('last_name', models.TextField()),
                ('number', models.TextField()),
            ],
        ),
        migrations.CreateModel(
            name='Activities',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.TextField()),
                ('limit', models.IntegerField()),
                ('date', models.DateField(default=datetime.datetime.now)),
                ('start_hour', models.CharField(default=12, max_length=2)),
                ('end_hour', models.CharField(default=14, max_length=2)),
                ('client_counter', models.IntegerField(default=0)),
                ('saved', models.ManyToManyField(blank=True, to='sportclub.Client')),
            ],
            options={
                'ordering': ['date', '-start_hour', '-end_hour'],
            },
        ),
    ]
