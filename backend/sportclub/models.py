from datetime import datetime

from django.db import models


class Client(models.Model):
    name = models.TextField()
    last_name = models.TextField()
    number = models.TextField()
    email = models.EmailField()
    is_confirmed = models.BooleanField(default=False)
    carnet = models.TextField(default='')


    def __str__(self):
        return f'{self.name}'


# Create your models here.
class Activities(models.Model):
    name = models.TextField()
    limit = models.IntegerField()
    date = models.DateField(default=datetime.now)
    start_hour = models.CharField(max_length=2, default=12)
    end_hour = models.CharField(max_length=2, default=14)
    saved = models.ManyToManyField(Client, blank=True)
    client_counter = models.IntegerField(default=0)
    #unique=True

    class Meta:
        ordering = ['date', 'start_hour', 'end_hour']

    def __str__(self):
        return f'{self.name}'
