from sportclub import models
from rest_framework import serializers


class ActivitySerializer(serializers.ModelSerializer):
    saved_count = serializers.SerializerMethodField()

    def get_saved_count(self, obj):
        return obj.saved.count()

    isFree = serializers.SerializerMethodField()

    def get_isFree(self, obj):
        if obj.saved.count() >= obj.limit:
            return False
        else:
            return True

    class Meta:
        model = models.Activities
        fields = '__all__'
        # exclude = ['saved']





class ClientSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.Client
        fields = ['id','name','last_name','number','email','is_confirmed','carnet']

class ActivitySavedSerializer(serializers.ModelSerializer):
    saved = ClientSerializer(many=True, read_only=True)
    class Meta:
        model = models.Activities
        fields = ['saved', 'client_counter']




