from django.contrib.auth.models import User, Group
from django.db.models import Count, Q, F
from django.http import JsonResponse
from rest_framework import viewsets
from rest_framework import permissions
from rest_framework.decorators import action
from rest_framework.response import Response
from django.core.mail import send_mail



from sportclub import serializers, models


# def validLimit(queryset):

class ActivityViewSet(viewsets.ModelViewSet):

    queryset = models.Activities.objects.all()
    serializer_class = serializers.ActivitySerializer

    def partial_update(self, request, *args, **kwargs):
        respone = super().partial_update(request)
        klient = models.Client.objects.filter(id=request.data['saved'][-1]).first()
        print(f'Wysylanie maila do {klient}....')
        return respone

    @action(detail=False, methods=['get'])
    def free(self, request):
        free_activity = models.Activities.objects.filter(client_counter__lt=F('limit'))

        serializer = serializers.ActivitySerializer(free_activity, many=True)
        return Response(serializer.data)

    @action(detail=False)
    def occupied(self, request):
        occupied_activity = models.Activities.objects.filter(client_counter__exact=F('limit'))
        serializer = serializers.ActivitySerializer(occupied_activity, many=True)
        return Response(serializer.data)

    @action(detail=True)
    def participants(self, request, pk=None):
        queyset = models.Activities.objects.filter(id=pk)
        serializer = serializers.ActivitySavedSerializer(queyset, many=True)
        return Response(serializer.data)

    @action(detail=True, methods=['get'])
    def noparticipants(self, request, pk=None):
        queyset = models.Client.objects.exclude(activities=pk).filter(is_confirmed=True)
        serializer = serializers.ClientSerializer(queyset, many=True)
        return Response(serializer.data)


class ClientModelViewSet(viewsets.ModelViewSet):
    queryset = models.Client.objects.all()
    serializer_class = serializers.ClientSerializer

    @action(detail=False, methods=['get'])
    def confirmed(self, request):
        queyset = models.Client.objects.filter(is_confirmed=True)
        serializer = serializers.ClientSerializer(queyset, many=True)
        return Response(serializer.data)


    @action(detail=True, methods=['get'])
    def savedOn(self, request, pk=True):
        queyset = models.Activities.objects.filter(saved__in=pk, saved__is_confirmed=True)
        serializer = serializers.ActivitySerializer(queyset, many=True)


        # serializer = serializers.ClientSerializer(queyset, many=True)
        return Response(serializer.data)
        #
