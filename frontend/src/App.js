import React from "react";
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";
import TabPanel from "./Components/TabPanel";
import Header from "./Components/Header";
import ClientForm from "./Components/ClientForm";

export default function App() {
  return (
    <Router>
      {/* A <Switch> looks through its children <Route>s and
            renders the first one that matches the current URL. */}
      <Switch>
        <Route path="/klient">
          <About />
        </Route>

        <Route path="/recepcja">
          <Home />
        </Route>
      </Switch>
    </Router>
  );
}
function Home() {
  return (
    <div>
      <Header title="Lista zajec"></Header>
      <TabPanel></TabPanel>
    </div>
  );
}
function About() {
  return <ClientForm></ClientForm>;
}
