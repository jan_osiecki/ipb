import { TextField } from "@material-ui/core";
import React from "react";
import { useField } from "formik";
import Grid from "@material-ui/core/Grid";

import { createMuiTheme } from "@material-ui/core/styles";
import { ThemeProvider } from "@material-ui/styles";
const theme = createMuiTheme({
  palette: {
    primary: {
      main: "rgb(78,142,245)",
    },
  },
});
const MyTextField = ({ placeholder, ...props }) => {
  const [field, meta] = useField(props);
  const error = meta.error && meta.touched ? meta.error : "";
  return (
    <Grid item xs={4} md={2}>
      <ThemeProvider theme={theme}>
        <TextField
          onChange={field.onChange}
          onBlur={field.onBlur}
          checked={field.checked}
          placeholder={placeholder}
          name={field.name}
          value={field.value}
          error={!!error}
          helperText={error}
          label={props.label}
        />
      </ThemeProvider>
    </Grid>
  );
};

export default MyTextField;
