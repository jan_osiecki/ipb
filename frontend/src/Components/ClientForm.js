import React from "react";
import { Formik, Form, Field, ErrorMessage } from "formik";
import { Button, TextField } from "@material-ui/core";
import * as Yup from "yup";
import FormLabel from "@material-ui/core/FormLabel";
import { useField, FormikProps } from "formik";
import FormikTextField from "./FormikTextField";
import Grid from "@material-ui/core/Grid";
import axios from "axios";
import { makeStyles } from "@material-ui/core/styles";
import { useState } from "react";

axios.defaults.baseURL = "http://127.0.0.1:8000/clients/";

// first_name last_name email pesel phone_number Street Town adress_no postal_cod

const validationSchema = Yup.object({
  first_name: Yup.string()
    .min(3, "Podaj imie")
    .max(20, "Podaj imie")
    .required("Wymagane"),
  last_name: Yup.string().required("Wymagane"),
  email: Yup.string().email("Invalid email").required("Wymagane"),
  pesel: Yup.string().required("Wymagane"),
  phone_number: Yup.string().required("Wymagane"),
  street_name: Yup.string().required("Wymagane"),
  city: Yup.string().required("Wymagane"),
  home_number: Yup.string().required("Wymagane"),
  apt_number: Yup.string(),
  post_code: Yup.string().required("Wymagane"),
});

const defaultValues = {
  first_name: "",
  last_name: "",
  email: "",
  pesel: "",
  phone_number: "",
  street_name: "",
  city: "",
  home_number: "",
  apt_number: "null",
  post_code: "",
};

const ClientForm = ({ form_data, closeFunction }) => {
  //console.log(data)
  const [done, setDone] = useState(false);

  const initalValues = form_data != null ? form_data : defaultValues;
  const onSubmit = (values) => {
    const {
      street_name,
      home_number,
      apt_number,
      post_code,
      city,
      first_name,
      last_name,
      pesel,
      email,
      phone_number,
    } = values;

    const data = {
      adress: {
        street_name: street_name,
        home_number: home_number,
        apt_number: apt_number,
        post_code: post_code,
        city: city,
        country: "POL",
      },
      first_name: first_name,
      last_name: last_name,
      pesel: pesel,
      email: email,
      phone_number: phone_number,
      prefix: "sir",
    };

    axios.post("", {
      name: first_name,
      last_name: last_name,
      number: phone_number,
      email: "aa@bb.pl",
    });
    setDone(true);
    setTimeout(function () {
      window.location.reload(false);
    }, 3000);

    closeFunction();
  };

  // const initalValues = initialValues;
  return (
    <div>
      <Formik
        initialValues={initalValues}
        onSubmit={onSubmit}
        validationSchema={validationSchema}
      >
        <Form>
          <Grid container spacing={5}>
            <FormikTextField
              name="first_name"
              label="Imie"
              placeholder="np. jan"
              InputProps={{
                readOnly: true,
              }}
            ></FormikTextField>

            <FormikTextField
              name="last_name"
              label="Nazwisko"
              placeholder="np. nazwisko"
              InputProps={{
                readOnly: true,
              }}
            ></FormikTextField>
          </Grid>

          <Grid container spacing={5}>
            <FormikTextField
              name="email"
              label="E-mail"
              placeholder="np.email@gmail.com"
              InputProps={{
                readOnly: true,
              }}
            ></FormikTextField>

            <FormikTextField
              name="pesel"
              label="Pesel"
              placeholder="np. 20010123876"
              InputProps={{
                readOnly: true,
              }}
            ></FormikTextField>
          </Grid>
          <Grid container spacing={5}>
            <FormikTextField
              name="phone_number"
              label="Nr. tel"
              placeholder="np. 123456789"
              InputProps={{
                readOnly: true,
              }}
            ></FormikTextField>

            <FormikTextField
              name="street_name"
              label="Ulica"
              placeholder="np. Kolorowa"
              InputProps={{
                readOnly: true,
              }}
            ></FormikTextField>
          </Grid>
          <Grid container spacing={1}>
            <FormikTextField
              name="apt_number"
              label="Numer mieszkania"
              placeholder="np. 15A"
              InputProps={{
                readOnly: true,
              }}
            ></FormikTextField>
            <FormikTextField
              name="home_number"
              label="Numer domu"
              placeholder="np. 15/1"
              InputProps={{
                readOnly: true,
              }}
            ></FormikTextField>
          </Grid>
          <Grid container spacing={5}>
            <FormikTextField
              name="city"
              label="Miasto"
              placeholder="np. Warszawa"
              InputProps={{
                readOnly: true,
              }}
            ></FormikTextField>
            <FormikTextField
              name="post_code"
              label="Kod pocztowy"
              placeholder="np. 02-123"
              InputProps={{
                readOnly: true,
              }}
            ></FormikTextField>
          </Grid>
          <div>
            <Button
              style={{ marginTop: "30px" }}
              variant="contained"
              color="primary"
              type="submit"
              disabled={done}
            >
              Potwierdz
            </Button>
          </div>
        </Form>
      </Formik>
      {done ? (
        <p>Gratuluje, zostales klientem naszego klubu sportowego!</p>
      ) : null}
    </div>
  );
};

export default ClientForm;
