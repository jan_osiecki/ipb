import React from "react";
import PropTypes from "prop-types";
import { makeStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import Typography from "@material-ui/core/Typography";
import Box from "@material-ui/core/Box";
import DanseTableClient from "./DanseTableClient";

function TabPanelClient(props) {
  const { children, value, index, idActiv, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

TabPanelClient.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

function a11yProps(index) {
  return {
    id: `simple-tab-${index}`,
    "aria-controls": `simple-tabpanel-${index}`,
  };
}

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper,
  },
}));

export default function SimpleTabs({ idActiv }) {
  const classes = useStyles();
  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
    <div className={classes.root}>
      <AppBar position="static">
        <Tabs
          value={value}
          onChange={handleChange}
          aria-label="simple tabs example"
        >
          <Tab label="Zapisani na zajecia" {...a11yProps(0)} />
          <Tab label="Dodaj do zajec" {...a11yProps(1)} />
        </Tabs>
      </AppBar>
      <TabPanelClient value={value} index={0}>
        <DanseTableClient
          url={`http://127.0.0.1:8000/activites/${idActiv}/participants/`}
          flag="n"
          idActiv={idActiv}
        ></DanseTableClient>
      </TabPanelClient>
      <TabPanelClient value={value} index={1}>
        <DanseTableClient
          url={`http://127.0.0.1:8000/activites/${idActiv}/noparticipants/`}
          flag="t"
          idActiv={idActiv}
        ></DanseTableClient>
      </TabPanelClient>
    </div>
  );
}
