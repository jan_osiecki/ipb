import React from "react";
import Header from "./Header";
import TabPanelClient from "./TabPanelClient";

export default ({ close, idActiv, reloadPage }) => {
  function updatePage(fun, reloadPage) {
    fun();
    window.location.reload();
  }
  return (
    <div className="modal">
      <a className="close" onClick={() => updatePage(close)}>
        &times;
      </a>
      <Header title="Zarzadzaj zajeciami"></Header>{" "}
      <TabPanelClient idActiv={idActiv}></TabPanelClient>
    </div>
  );
};
