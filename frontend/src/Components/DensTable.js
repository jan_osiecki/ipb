import React, { useEffect, useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import axios from "axios";
import Button from "@material-ui/core/Button";
import Popup from "reactjs-popup";
//import Content from "./Content";
import ClientForm from "./ClientForm";
import ClientModal from "./ClientModal";
import Grid from "@material-ui/core/Grid";

const useStyles = makeStyles({
  table: {
    minWidth: 650,
  },
  buttons: {
    display: "flex",
    justifyContent: "space-between",
  },
});

function createData(id, name, last_name, email, number, carnet) {
  return {
    id,
    name,
    last_name,
    email,
    number,
    carnet,
  };
}

let rows = [];
axios.defaults.baseURL = "http://127.0.0.1:8000/clients/";

export default function DenseTable({ url }) {
  const classes = useStyles();
  const [activites, setActivites] = useState([]);
  const [formAdd, setFormAdd] = useState(false);
  const [formUpdate, setFormUpdate] = useState(false);
  const [initialValues, setInitialValues] = useState({});

  useEffect(() => {
    reloadPage();
  }, []);

  function removeClient(id) {
    axios.delete(`${id}/`);
  }

  function reloadPage() {
    axios.get(url).then((res) => {
      res.data.map((obj) => {
        rows.push(
          createData(
            obj.id,
            obj.name,
            obj.last_name,
            obj.email,
            obj.number,
            obj.carnet
          )
        );
      });
      setActivites(rows);
      rows = [];
    });
  }

  return (
    <div>
      <TableContainer component={Paper}>
        <Table
          className={classes.table}
          size="small"
          aria-label="a dense table"
        >
          <TableHead>
            <TableRow>
              <TableCell>Imie</TableCell>
              <TableCell align="right">Nazwisko</TableCell>
              <TableCell align="right">numer telefonu</TableCell>
              <TableCell align="right">email</TableCell>
              <TableCell align="right">karnet</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {activites.map((row) => (
              <TableRow key={row.name}>
                <TableCell component="th" scope="row">
                  {row.name}
                </TableCell>
                <TableCell align="right">{row.last_name}</TableCell>
                <TableCell align="right">{row.number}</TableCell>
                <TableCell align="right">{row.email}</TableCell>
                <TableCell align="right">{row.carnet}</TableCell>

                <TableCell align="right">
                  <Grid md={5} xl={4} className={classes.buttons}>
                    <Button
                      variant="contained"
                      color="primary"
                      onClick={() => {
                        setFormUpdate(!formUpdate);
                        setInitialValues(row);
                      }}
                    >
                      Zarzadzaj
                    </Button>
                  </Grid>
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
      <Button
        variant="contained"
        color="primary"
        onClick={() => {
          setFormAdd(!formAdd);
        }}
      >
        Add
      </Button>
      {formAdd ? (
        <ClientModal data={null} setForm={setFormAdd} title="Dodaj klienta" />
      ) : null}
      {formUpdate ? (
        <ClientModal
          data={initialValues}
          setForm={setFormUpdate}
          title="Edytuj dane"
        />
      ) : null}
    </div>
  );
}
