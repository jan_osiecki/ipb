import React, { useState, useEffects, useEffect } from "react";
import axios from "axios";

function FetchApi() {
  const [posts, setPosts] = useState([]);
  useEffect(() => {
    axios.get("http://127.0.0.1:8000/activites/").then((res) => {
      console.log(res);
      setPosts(res.data);
    });
  }, []);
  return (
    <div>
      {posts.map((post) => {
        return (
          <p>
            {post.id},{post.name},{post.limit},{post.isFree.toString()}
          </p>
        );
      })}
    </div>
  );
}

export default FetchApi;
