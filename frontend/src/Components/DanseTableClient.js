import React, { useEffect, useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import axios from "axios";
import Button from "@material-ui/core/Button";

const useStyles = makeStyles({
  table: {
    minWidth: 650,
  },
});

function createData(name, lastName, phone, id) {
  return { name, lastName, phone, id };
}

let rows = [];
let newSaved;
export default function DanseTableClient({ url, flag, idActiv, handleChange }) {
  const classes = useStyles();
  const [clients, setClients] = useState([]);
  const [buttonFlag, setbuttonFlag] = useState([true]);
  let client;
  let limitActiv;
  let clientCounter;

  useEffect(() => {
    axios.get(`http://127.0.0.1:8000/activites/${idActiv}/`).then((res) => {
      limitActiv = res.data.limit;
      clientCounter = res.data.client_counter;
    });
    axios.get(url).then((res) => {
      if (flag === "n") {
        client = res.data[0]["saved"];
      } else {
        client = res.data;
      }
      setTimeout(() => {
        console.log(limitActiv);
        console.log(clientCounter);
        if (limitActiv == clientCounter) {
          setbuttonFlag(false);
        }
      }, 100);

      client.map((obj) => {
        rows.push(createData(obj.name, obj.last_name, obj.number, obj.id));
      });
      setClients(rows);
      rows = [];
    });
  }, []);
  let limit;
  let savedCount;
  let client_count;
  function handleClick(row) {
    axios.get(`http://127.0.0.1:8000/activites/${idActiv}/`).then((res) => {
      newSaved = res.data.saved;
      client_count = res.data.client_counter + 1;
      newSaved.push(row);
      limit = res.data.limit;
      savedCount = newSaved.length;
      console.log(limit);
      console.log(savedCount);
      axios.patch(`http://127.0.0.1:8000/activites/${idActiv}/`, {
        saved: newSaved,
        client_counter: client_count,
      });
      if (limit == savedCount) {
        setbuttonFlag(false);
      }
    });
    setTimeout(() => {
      axios
        .get(`http://127.0.0.1:8000/activites/${idActiv}/noparticipants/`)
        .then((res) => {
          client = res.data;

          console.log(client);
          client.map((obj) => {
            console.log(obj.name);
            rows.push(createData(obj.name, obj.last_name, obj.number, obj.id));
          });
          setClients(rows);
          rows = [];
        });
    }, 200);
  }

  return (
    <TableContainer component={Paper}>
      <Table className={classes.table} size="small" aria-label="a dense table">
        <TableHead>
          <TableRow>
            <TableCell align="right">Id</TableCell>
            <TableCell align="right">Imie</TableCell>
            <TableCell align="right">Nazwisko</TableCell>
            <TableCell align="right">Telefon</TableCell>
            <TableCell align="right">Dodaj</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {clients.map((row) => (
            <TableRow key={row.id}>
              <TableCell component="th" scope="row" align="right">
                {row.id}
              </TableCell>
              <TableCell align="right">{row.name}</TableCell>

              <TableCell align="right">{row.lastName}</TableCell>
              <TableCell align="right">{row.phone}</TableCell>

              <TableCell align="right">
                {flag === "t" && buttonFlag ? (
                  <Button
                    variant="contained"
                    color="primary"
                    onClick={() => handleClick(row.id)}
                  >
                    Dodaj
                  </Button>
                ) : (
                  <Button variant="contained" disabled>
                    Dodaj
                  </Button>
                )}
              </TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
}
