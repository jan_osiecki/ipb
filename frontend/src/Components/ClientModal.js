import React from "react";
import Modal from "@material-ui/core/Modal";
import { makeStyles } from "@material-ui/core/styles";
import ClientForm from "./ClientForm";
import { Typography } from "@material-ui/core";
import TextField from "@material-ui/core/TextField";
import Autocomplete from "@material-ui/lab/Autocomplete";
import Button from "@material-ui/core/Button";
import axios from "axios";

const useStyles = makeStyles((theme) => ({
  app: {
    display: "flex",
    padding: theme.spacing(1),
    alignItems: "center",
    justifyContent: "center",
    //position: "absolute",
    //verticalAlign: "center",
  },
  title: {},
  form: {
    backgroundColor: theme.palette.background.paper,
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
    outline: 0,
  },
}));

const ClientModal = ({ data, setForm, title }) => {
  const [open, setOpen] = React.useState(true);
  const [val, setVal] = React.useState("");

  const submitClose = () => {
    setOpen(false);
    setForm(false);
  };

  const handleOnClose = () => {
    setOpen(false);
    setForm(false);
  };

  const classes = useStyles();
  const carnets = [
    { name: "Karnet 5 dni w tygodniu" },
    { name: "Karnet 7 dni w tygodniu" },
    { name: "Karnet weekendowy" },
    { name: "Karnet poranny" },
  ];
  return (
    <Modal
      className={classes.app}
      onBackdropClick={handleOnClose}
      onSubmit={handleOnClose}
      open={open}
    >
      <div className={classes.form}>
        <Typography variant="h4" align="center">
          <Autocomplete
            id="combo-box-demo"
            options={carnets}
            getOptionLabel={(option) => option.name}
            style={{ width: 300 }}
            onInputChange={(event, newInputValue) => {
              setVal(newInputValue);
            }}
            renderInput={(params) => (
              <TextField {...params} label="Combo box" variant="outlined" />
            )}
          />
        </Typography>
        <Button
          variant="contained"
          color="primary"
          onClick={() => {
            console.log(data.id);
            axios.patch(`http://127.0.0.1:8000/clients/${data.id}/`, {
              is_confirmed: true,
              carnet: val,
            });
            setTimeout(function () {
              window.location.reload(false);
            }, 500);
            //window.location.reload(false);
            handleOnClose();
          }}
        >
          Potwierdz
        </Button>
      </div>
    </Modal>
  );
};

export default ClientModal;
