import React, { useEffect, useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import axios from "axios";
import Button from "@material-ui/core/Button";
import Popup from "reactjs-popup";
import Content from "./Content";

const useStyles = makeStyles({
  table: {
    minWidth: 650,
  },
});

function createData(
  name,
  limit,
  isFree,
  date,
  hourStart,
  hourEnd,
  id,
  saved_count
) {
  return { name, limit, isFree, date, hourStart, hourEnd, id, saved_count };
}

let rows = [];

export default function DenseTable({ url }) {
  const classes = useStyles();
  const [activites, setActivites] = useState([]);
  useEffect(() => {
    axios.get(url).then((res) => {
      console.log(res);
      res.data.map((obj) => {
        rows.push(
          createData(
            obj.name,
            obj.limit,
            obj.isFree,
            obj.date,
            obj.start_hour,
            obj.end_hour,
            obj.id,
            obj.saved_count
          )
        );
      });
      setActivites(rows);
      rows = [];
    });
  }, []);

  function reloadPage() {
    axios.get(url).then((res) => {
      console.log(res);
      res.data.map((obj) => {
        rows.push(
          createData(
            obj.name,
            obj.limit,
            obj.isFree,
            obj.date,
            obj.start_hour,
            obj.end_hour,
            obj.id,
            obj.saved_count
          )
        );
      });
      setActivites(rows);
      rows = [];
    });
  }

  return (
    <TableContainer component={Paper}>
      <Table className={classes.table} size="small" aria-label="a dense table">
        <TableHead>
          <TableRow>
            <TableCell>Nazwa zajec</TableCell>
            <TableCell align="right">Limit osob</TableCell>
            <TableCell align="right">Godziny</TableCell>
            <TableCell align="right">Data</TableCell>
            <TableCell align="right">Wolne/zajete</TableCell>
            <TableCell align="right">Dodaj klienta</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {activites.map((row) => (
            <TableRow key={row.name}>
              <TableCell component="th" scope="row">
                {row.name}
              </TableCell>

              <TableCell align="right">
                {row.saved_count}/{row.limit}
              </TableCell>
              <TableCell align="right">
                {row.hourStart}-{row.hourEnd}
              </TableCell>
              <TableCell align="right">{row.date}</TableCell>
              <TableCell align="right">
                {row.isFree ? "wolne" : "zajete"}
              </TableCell>
              <TableCell align="right">
                {row.isFree ? (
                  <Popup
                    modal
                    trigger={
                      <Button variant="contained" color="primary">
                        Dodaj
                      </Button>
                    }
                  >
                    {(close) => (
                      <Content
                        close={close}
                        idActiv={row.id}
                        reloadPage={reloadPage}
                      />
                    )}
                  </Popup>
                ) : (
                  <Button variant="contained" disabled>
                    Dodaj
                  </Button>
                )}
              </TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
}
